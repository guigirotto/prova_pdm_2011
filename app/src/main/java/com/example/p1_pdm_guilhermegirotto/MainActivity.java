package com.example.p1_pdm_guilhermegirotto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;


@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity {


    private ArrayList<AddRegistro> dados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




    }

    public void AddRegistro(View view){
        Intent intent = new Intent(this, Registro.class);
        startActivityForResult(intent,1);

    }
    public void MostrarRegistro(View view){
        Intent intent = new Intent(this, Mostrar.class);
        startActivity(intent);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        TextView saldo = findViewById(R.id.saldo);
        String add = ""+AddRegistroDAO.getINSTANCE().saldo();
        add = AddRegistroDAO.getINSTANCE().saldo()<0?"-R$"+add.substring(1,add.length()):"R$"+add;
        saldo.setText(add);


    }





}