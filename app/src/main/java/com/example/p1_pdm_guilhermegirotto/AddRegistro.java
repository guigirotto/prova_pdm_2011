package com.example.p1_pdm_guilhermegirotto;

public class AddRegistro implements Comparable<AddRegistro>{
    private String c_trans;
    private String data;
    private String  nome;
    private double valor;

    @Override
    public String toString() {
        return "AddRegistro{" +
                "c_trans='" + c_trans + '\'' +
                ", data='" + data + '\'' +
                ", nome='" + nome + '\'' +
                ", valor=" + valor +
                '}';
    }

    public String getC_trans() {
        return c_trans;
    }

    public void setC_trans(String c_trans) {
        this.c_trans = c_trans;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    @Override
    public int compareTo(AddRegistro o) {
        return this.getC_trans().toUpperCase().compareTo(o.getC_trans().toUpperCase());
    }
}
