package com.example.p1_pdm_guilhermegirotto;

import java.util.ArrayList;

public class AddRegistroDAO {
    private static AddRegistroDAO INSTANCE;
    private ArrayList<AddRegistro> registros;

    public AddRegistroDAO() {
        this.registros = new ArrayList<>();
    }

    public static final AddRegistroDAO getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new AddRegistroDAO();
        }
        return INSTANCE;
    }

    public ArrayList<AddRegistro> getRegistros() {
        return registros;
    }

    public void setRegistros(AddRegistro registros) {
        this.registros.add(registros);
    }

    public double saldo() {
        double saldo = 0;
        for (AddRegistro a : this.registros) {
            if (a.getC_trans() == "Crédito")
                saldo = saldo + a.getValor();
            else
                saldo = saldo - a.getValor();
        }
            return saldo;

        }
}

