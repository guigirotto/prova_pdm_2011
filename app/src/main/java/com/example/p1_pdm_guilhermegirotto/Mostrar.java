package com.example.p1_pdm_guilhermegirotto;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class Mostrar extends AppCompatActivity {
    RecyclerView mostrar;
    AddRegistroAdapter a_mostrar;
    ArrayList<AddRegistro> registros;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        mostrar = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mostrar.setLayoutManager(linearLayoutManager);
        registros = AddRegistroDAO.getINSTANCE().getRegistros();
        a_mostrar = new AddRegistroAdapter(registros);
        mostrar.setAdapter(a_mostrar);
    }


    public void sorting(View v){
        Collections.sort(registros);
        a_mostrar = new AddRegistroAdapter(registros);
        mostrar.setAdapter(a_mostrar);
    }

    public void export(View view){
        StringBuilder data = new StringBuilder();

        data.append("\n"+registros);

        try {
            FileOutputStream out = openFileOutput("dados_transações.csv", Context.MODE_PRIVATE);
            out.write((data.toString().getBytes()));
            out.close();


            Context context = getApplicationContext();
            File filelocation = new File(getFilesDir(), "dados_transações.csv");
            Uri path = FileProvider.getUriForFile(context, "com.example.exportcsv.fileprovider", filelocation);
            Intent fileIntent = new Intent(Intent.ACTION_SEND);
            fileIntent.setType("text/csv");
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Dados");
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            fileIntent.putExtra(Intent.EXTRA_STREAM, path);
            startActivity(Intent.createChooser(fileIntent, "Salvar"));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
