package com.example.p1_pdm_guilhermegirotto;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Registro  extends AppCompatActivity {

    Spinner l_transacoes;
    List lista;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //=================== Incluindo itens no spinner
        l_transacoes = findViewById(R.id.transacao);
        lista = new ArrayList<String>();
        lista.add("Crédito");
        lista.add("Débito");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, lista);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        l_transacoes.setAdapter(spinnerAdapter);
    }

    public boolean verifica_data(String strDate){

        if (strDate.matches("([0-9]{2})/([0-9]{2})/([0-9]{4})"))
            return true;
        else
            return false;

    }



    public void add_registro(View View){
        // pegando itens informados nos campos
        EditText nome_transacao = findViewById(R.id.nome_transacao);
        EditText data = findViewById(R.id.data_transacao);
        EditText valor = findViewById(R.id.valor);

        if(nome_transacao.getText().toString()+"" != "" && valor.getText().toString()+"" != "" && data.getText().toString()+"" != "" && verifica_data(data.getText().toString())){
            AddRegistro add = new AddRegistro();
            int pos = l_transacoes.getSelectedItemPosition();
            String tipo = (String) lista.get(pos);

            //=================== SETANDO VALORES================

            add.setC_trans(tipo);
            add.setData(data.getText().toString());
            add.setValor((Double.valueOf(valor.getText().toString())));
            add.setNome(nome_transacao.getText().toString());

            AddRegistroDAO.getINSTANCE().setRegistros(add);
            finish();

            Toast.makeText(this, "Registro Adicionado com sucesso !",
                    Toast.LENGTH_LONG).show();




        }else{
            Toast.makeText(this, "Dados informados incorreto, por favor tente novamente !",
                    Toast.LENGTH_LONG).show();
        }

    }
}
