package com.example.p1_pdm_guilhermegirotto;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AddRegistroAdapter extends RecyclerView.Adapter<AddRegistroAdapter.ViewHolderAddRegistro> {

    private ArrayList<AddRegistro> dados;
    public AddRegistroAdapter(ArrayList<AddRegistro> d) { dados = d;}
    @NonNull
    @Override

    public  AddRegistroAdapter.ViewHolderAddRegistro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_mostrar, parent,false);
        ViewHolderAddRegistro h_registro = new  ViewHolderAddRegistro(view);
        return h_registro;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderAddRegistro holder, int position) {
        if (dados !=null && dados.size()>0){
            AddRegistro p = dados.get(position);
            holder.c_trans.setText(p.getC_trans());
            holder.data.setText(p.getData());
            holder.nome.setText(p.getNome());
            holder.valor.setText("R$"+p.getValor());
            holder.cor.setBackgroundColor(p.getC_trans()=="Crédito"? Color.GREEN:Color.RED);

        }

    }

    @Override
    public int getItemCount() {return dados.size();}

    public class ViewHolderAddRegistro extends RecyclerView.ViewHolder{
        public TextView c_trans, data, nome, valor;
        public ImageView cor;
        public ViewHolderAddRegistro(@NonNull View itemView) {
            super(itemView);
            c_trans = itemView.findViewById(R.id.tipo);
            nome = itemView.findViewById(R.id.desc);
            data = itemView.findViewById(R.id.date);
            valor = itemView.findViewById(R.id.price);
            cor = itemView.findViewById(R.id.cor_trans);

        }
    }
}
